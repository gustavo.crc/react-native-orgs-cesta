import { useFonts, Montserrat_400Regular, Montserrat_700Bold } from '@expo-google-fonts/montserrat';
import AppLoading from 'expo-app-loading';
import Cesta from './src/telas/Cesta';

import mock from './src/mocks/cesta'
import { SafeAreaView } from 'react-native';
import { StatusBar } from 'expo-status-bar';

export default function App() {
  const [fonteCarregada] = useFonts({
    "MontSerratRegular": Montserrat_400Regular,
    "MontSerratBold": Montserrat_700Bold
  });

  if (!fonteCarregada) {
    return (
      <AppLoading />
    )
  }

  return <>
    <SafeAreaView style={{ flex: 1 }} >
      <StatusBar />
      {/* Ao passar {...mock} estamos removendo a camada externa do objeto, assim não precisa declarar
      exemplo: topo={mock.topo} e detalhe={mock.detalhe} */}
      <Cesta {...mock} />
    </SafeAreaView>
  </>
}