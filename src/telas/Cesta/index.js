import React from "react";
import { FlatList, StyleSheet, View } from "react-native";

import Topo from "./componentes/Topo"
import Detalhes from "./componentes/Detalhes";
import Item from "./componentes/Item";
import Texto from "../../componentes/Texto";

export default function Cesta({ topo, detalhes, itens }) {
  return <>
    <FlatList // componente pra renderizar grandes conjuntos de dados de forma eficiente
      data={itens.lista} // fonte de dados da lista
      renderItem={Item} // função que define como cada item da lista deve ser renderizado
      keyExtractor={({ nome }) => nome} // especifica como as chaves únicas de cada item da lista devem ser extraídas.
      ListHeaderComponent={() => { // componente que é renderizado acima da lista
        return <>
          <Topo {...topo} />
          <View style={estilos.cesta}>
            <Detalhes {...detalhes} />
            <Texto style={estilos.titulo}>{itens.titulo}</Texto>
          </View>
        </>
      }}
    />
  </>
}

const estilos = StyleSheet.create({
  titulo: {
    color: "#464646",
    fontWeight: "bold",
    marginTop: 32,
    marginBottom: 8,
    fontSize: 20,
    lineHeight: 32
  },
  cesta: {
    paddingVertical: 8, // React native não permite adicionar mais de um parametro em uma propriedade.
    paddingHorizontal: 16 // Utilizado de forma separada pro texto não ficar colado nas bordas.
  }
});