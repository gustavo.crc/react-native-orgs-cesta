import React from "react";
import { Image, StyleSheet, Dimensions, StatusBar } from "react-native";

import Texto from "../../../componentes/Texto";

// Saber a largura da tela
const widht = Dimensions.get("screen").width;

export default function Topo({ imagem, titulo }) {
  return <>
    <Image source={imagem} style={estilos.topo} />
    <Texto style={estilos.titulo}>{titulo}</Texto>
  </>
}

const estilos = StyleSheet.create({
  topo: {
    width: "100%",
    height: 578 / 768 * widht,
    // 578 é a altura da imagem, 768 é a largura, e multiplicamos pela largura da tela.
  },
  titulo: {
    width: "100%",
    position: "absolute",
    textAlign: "center",
    fontSize: 16,
    lineHeight: 26, // Espaço entre as linhas de texto
    color: "white",
    fontWeight: "bold", // Negrito
    padding: 6
  }
})