import React from "react";
import { View, Image, StyleSheet } from "react-native";

import Texto from "../../../componentes/Texto";

export default function Item({ item: { nome, imagem } }) {
  // esta variavel é uma função, e essa função está sem as {} pra que não precise usar o return e sim retorne direto o codigo
  return <View style={estilos.item}>
    <Image style={estilos.image} source={imagem} />
    <Texto style={estilos.nome}>{nome}</Texto>
  </View>
}

const estilos = StyleSheet.create({
  item: {
    flexDirection: "row", // Permite deixar a imagem ao lado do texto
    borderBottomWidth: 1,
    borderBottomColor: "#ECECEC",
    paddingVertical: 16,
    marginHorizontal: 16,
    alignItems: "center"
  },
  imagem: {
    width: 46,
    height: 46
  },
  nome: {
    fontSize: 16,
    lineHeight: 26,
    marginLeft: 11,
    color: "#464646"
  }
})