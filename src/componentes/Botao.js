import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import Texto from "./Texto";

export default function Botao({ titulo, onPress, style }) {
  // TouchableOpacity torna a experiencia mais amigável, pois ele fica opaco quando clica
  return <TouchableOpacity style={[estilos.botao, style]} onPress={onPress}>
    <Texto style={estilos.textoBotao}>{titulo}</Texto>
  </TouchableOpacity>
}

const estilos = StyleSheet.create({
  botao: {
    marginTop: 16,
    backgroundColor: "#2A9F85",
    paddingVertical: 16,
    borderRadius: 6
  },
  textoBotao: {
    textAlign: "center",
    color: "white",
    fontSize: 16,
    lineHeight: 26,
    fontWeight: "bold"
  }
})